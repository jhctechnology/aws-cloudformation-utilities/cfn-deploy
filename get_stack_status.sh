#!/bin/bash -e

get_stack_status() {
  # always take the first element from the array as this is the most recent
  get_status_command="aws cloudformation list-stacks --query 'StackSummaries[?StackName==\`$CFN_STACK_NAME\`] | [0].StackStatus'"
  stack_status_raw=$(eval $get_status_command)
  stack_status=${stack_status_raw//\"/}
}
