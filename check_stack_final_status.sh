#!/bin/bash -e

if [ -L /usr/bin/get_stack_status ]; then
  . /usr/bin/get_stack_status
else
  . $CFN_DEPLOY_PATH/get_stack_status.sh
fi

check_stack_final_status() {
  get_stack_status
  case $stack_status in
  # Successful end states
  "CREATE_COMPLETE")
    echo "$CFN_STACK_NAME has been successfully created."
    ;;
  "UPDATE_COMPLETE")
    echo "$CFN_STACK_NAME has been successfully updated."
    ;;
  "DELETE_COMPLETE")
    echo "$CFN_STACK_NAME has been successfully deleted."
    ;;
  # Failed end states
  "UPDATE_ROLLBACK_COMPLETE"|"UPDATE_ROLLBACK_FAILED"|"DELETE_COMPLETE" \
  |"ROLLBACK_COMPLETE"|"CREATE_FAILED"|"ROLLBACK_FAILED"|"DELETE_FAILED")
    echo "$CFN_STACK_NAME is in failed end state $stack_status."
    exit 1
    ;;
  # Non end states
  "CREATE_IN_PROGRESS"|"DELETE_IN_PROGRESS"|"DELETE_IN_PROGRESS" \
  |"REVIEW_IN_PROGRESS"|"ROLLBACK_IN_PROGRESS"|"UPDATE_ROLLBACK_IN_PROGRESS" \
  |"UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS"|"UPDATE_IN_PROGRESS" \
  |"UPDATE_COMPLETE_CLEANUP_IN_PROGRESS")
    # we should never get here because of cloudformation_tail, but in case we do...
    echo "$CFN_STACK_NAME is in non-end state $stack_status."
    exit 1
    ;;
  *)
    # in case there's some state not listed here, assume failure
    echo "$CFN_STACK_NAME is in unknown state $stack_status."
    exit 1
    ;;
  esac
}
