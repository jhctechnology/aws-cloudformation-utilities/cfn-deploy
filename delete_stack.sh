#!/bin/bash -e

if [ -L /usr/bin/check_stack_final_status ]; then
  . /usr/bin/check_stack_final_status
else
  . $CFN_DEPLOY_PATH/check_stack_final_status.sh
fi

delete_stack() {
  aws cloudformation update-termination-protection --no-enable-termination-protection --stack-name $CFN_STACK_NAME
  aws cloudformation delete-stack --stack-name $CFN_STACK_NAME
  cfn-tail $CFN_STACK_NAME
  check_stack_final_status
}
