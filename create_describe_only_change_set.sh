#!/bin/bash

if [ -L /usr/bin/delete_change_set ]; then
  . /usr/bin/delete_change_set
else
  . $CFN_DEPLOY_PATH/delete_change_set.sh
fi

if [ -L /usr/bin/create_change_set ]; then
  create_change_set
else
  ./$CFN_DEPLOY_PATH/create_change_set.sh
fi

detect_changes_exit_code=$?
delete_change_set
exit $detect_changes_exit_code
