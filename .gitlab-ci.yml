---
image: $DOCKER_ENV_CI_REGISTRY/chs-library/docker/awslint:cfn-utils

variables:
  GIT_SUBMODULE_STRATEGY: normal
  AWS_DEFAULT_REGION: $REGION

  CFN_BUILD_PATH: ./cfn-build
  CFN_TEST_PATH: ./cfn-test
  CFN_DEPLOY_PATH: "."

  CFN_STAGING_S3_BUCKET: $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME-bucket
  CFN_STAGING_S3_BUCKET_TEMP: $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME-temp-bucket
  CFN_STAGING_S3_PREFIX: ""
  CFN_TEMPLATE_PATH: s3://cloudformation-templates-us-west-2
  CFN_BASE_TEMPLATE: https://s3-$REGION.amazonaws.com/$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME-bucket/${CFN_STAGING_S3_PREFIX}EC2InstanceWithSecurityGroupSample.template
  CFN_STACK_NAME: cfn-deploy-test-stack
  CFN_ROLE_ARN: arn:aws:iam::146313547956:role/csr-CloudFormation
  # CFN_CAPABILITIES:
  CFN_CHANGE_SET_NAME: CIChangeSet

.aws_before_script: &aws_before_script
  # set aws cli default region
  aws configure set default.region $REGION

.bash_test_template: &bash_test_template
  stage: test
  script:
    - echo "Checking $scriptname..."
    - bash -n $scriptname

.test_job_template: &test_job_template
  stage: test
  before_script:
    # make executable
    - chmod a+x $CFN_TEST_PATH/*.sh

.cfn_deploy_template: &cfn_deploy_template
  before_script:
    - *aws_before_script
    # make all bash scripts executable
    - chmod a+x $CFN_DEPLOY_PATH/*.sh

stages:
  - build
  - test
  - scan
  - staging
  - change_management
  - deploy
  - cleanup

cleanup_failed_change_sets:
  before_script:
    - *aws_before_script
  stage: build
  script:
    - chmod a+x $CFN_BUILD_PATH/*.sh
    - $CFN_BUILD_PATH/cleanup_failed_change_sets.sh

temp_stage_templates:
  before_script:
    - *aws_before_script
  stage: build
  script:
    - chmod a+x $CFN_BUILD_PATH/*.sh
    - $CFN_BUILD_PATH/stage_templates_s3.sh $CFN_STAGING_S3_BUCKET_TEMP

bash_test_check_stack_final_status:
  variables:
    scriptname: check_stack_final_status.sh
  <<: *bash_test_template

bash_test_create_change_set:
  variables:
    scriptname: create_change_set.sh
  <<: *bash_test_template

bash_test_create_describe_only_change_set:
  variables:
    scriptname: create_describe_only_change_set.sh
  <<: *bash_test_template

bash_test_delete_change_set:
  variables:
    scriptname: delete_change_set.sh
  <<: *bash_test_template

bash_test_delete_stack:
  variables:
    scriptname: delete_stack.sh
  <<: *bash_test_template

bash_test_detect_changes:
  variables:
    scriptname: detect_changes.sh
  <<: *bash_test_template

bash_test_execute_change_set:
  variables:
    scriptname: execute_change_set.sh
  <<: *bash_test_template

bash_test_get_stack_status:
  variables:
    scriptname: get_stack_status.sh
  <<: *bash_test_template

debug_params:
  stage: test
  script:
    - env

test_yaml:
  <<: *test_job_template
  script:
    - $CFN_TEST_PATH/validate-yaml.sh
    
test_json:
  <<: *test_job_template
  script:
    - $CFN_TEST_PATH/validate-json.sh

test_template:
  <<: *test_job_template
  script:
    - $CFN_TEST_PATH/validate-templates.sh $CFN_STAGING_S3_BUCKET_TEMP

describe_only_change_set:
  <<: *test_job_template
  variables:
    CFN_BASE_TEMPLATE: https://s3-$REGION.amazonaws.com/$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME-temp-bucket/${CFN_STAGING_S3_PREFIX}EC2InstanceWithSecurityGroupSample.template
  script:
    - $CFN_DEPLOY_PATH/create_describe_only_change_set.sh
  allow_failure: true

cleanup_temp_stage_templates:
  before_script:
    - *aws_before_script
  stage: staging
  script:
    - aws s3 rm s3://$CFN_STAGING_S3_BUCKET_TEMP/ --recursive
    - aws s3 rb s3://$CFN_STAGING_S3_BUCKET_TEMP

##=============================================================================
##  Deployment jobs
##
##  All jobs from this point issue actions which impact a live environment.
##  The stage_templates job begins by pushing CloudFormation templates to the
##  live S3 bucket. To prevent these steps from being executed without review
##  and approval, set these options for stage_templates:
##
##  when: manual
##  allow_failure: false
##
##  These options ensure that the staging stage and all future stages are not
##  fully executed without a manual trigger.
##=============================================================================
stage_templates:
  before_script:
    - *aws_before_script
  stage: staging
  script:
    - chmod a+x $CFN_BUILD_PATH/*.sh
    - $CFN_BUILD_PATH/stage_templates_s3.sh $CFN_STAGING_S3_BUCKET
  when: manual
  allow_failure: false

change_set:
  <<: *cfn_deploy_template
  stage: change_management
  script:
    - $CFN_DEPLOY_PATH/create_change_set.sh
  allow_failure: true
    
deploy:
  <<: *cfn_deploy_template
  stage: deploy
  script:
    - $CFN_DEPLOY_PATH/execute_change_set.sh

cleanup:
  <<: *cfn_deploy_template
  stage: cleanup
  script:
    - . $CFN_DEPLOY_PATH/delete_stack.sh
    - delete_stack
